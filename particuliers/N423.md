# Santé, protection sociale à l'étranger

* [Vacances ou court séjour à l'étranger](F213.md)
* [Installation à l'étranger](F407.md)
* [Recommandations sanitaires aux voyageurs](F1109.md)
* [Vaccinations internationales](F720.md)

----

<small>Cette fiche est issue de [service-public.fr](https://www.service-public.fr/particuliers/vosdroits/N423). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/service-public-vos-droits-markdown) utilisant les [données publiques de la Direction de l'information légale et administrative (DILA)](https://www.service-public.fr/a-propos/donnees-ouvertes).</small>
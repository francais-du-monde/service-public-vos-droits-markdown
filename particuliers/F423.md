# Français en Europe : faire venir sa famille

<p>Si vous êtes Français et partez vivre dans un autre pays de l'Espace économique européen (EEE) ou en Suisse, vous pouvez être accompagné  par votre famille.
		Vous devez disposer d'un droit au séjour dans ce pays. Les formalités d'installation des personnes de votre famille seront différentes suivant leur nationalité, française ou européenne ou non européenne. Après 5 ans de résidence, votre famille pourra bénéficier d'un droit au séjour permanent  dans le pays d'accueil.</p>

----

<small>Cette fiche est issue de [service-public.fr](https://www.service-public.fr/particuliers/vosdroits/F423). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/service-public-vos-droits-markdown) utilisant les [données publiques de la Direction de l'information légale et administrative (DILA)](https://www.service-public.fr/a-propos/donnees-ouvertes).</small>
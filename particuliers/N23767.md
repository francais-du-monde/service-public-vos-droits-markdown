# Scolarité à l'étranger

* [En Europe](F406.md)
* [Dans un autre pays](F32800.md)

## Et aussi sur francais-du-monde.fr

* [Résider à l'étranger](N120.md)

----

<small>Cette fiche est issue de [service-public.fr](https://www.service-public.fr/particuliers/vosdroits/N23767). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/service-public-vos-droits-markdown) utilisant les [données publiques de la Direction de l'information légale et administrative (DILA)](https://www.service-public.fr/a-propos/donnees-ouvertes).</small>